#!/bin/bash

set -e

export GOROOT=/usr/local/go
export GOPATH=/home/vagrant/go
export PATH="$GOROOT/bin:$GOPATH/bin:$PATH"
export CILIUM_USE_ENVOY=1

go get -u github.com/golang/protobuf/protoc-gen-go

mkdir -p /home/vagrant/go/src/github.com/cilium
cd /home/vagrant/go/src/github.com/cilium

git clone -b master https://github.com/cilium/cilium.git
cd cilium
git submodule update --init --recursive

cd envoy
make && make tests
